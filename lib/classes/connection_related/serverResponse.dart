class ServerResponse {
  bool success;
  String message;
  dynamic data;

  ServerResponse({this.success = false, this.data, this.message});
}
