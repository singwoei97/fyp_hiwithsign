class Feedback {
  int id;
  int userId;
  String feedbackMessage;
  String dateSent;

  Feedback({this.id, this.userId, this.feedbackMessage, this.dateSent});
}
