import 'package:fyp_hiwithsign/classes/model/video.dart';

class Sign {
  String signName;
  String category;
  Video video;

  Sign({this.signName, this.category, this.video});
}
