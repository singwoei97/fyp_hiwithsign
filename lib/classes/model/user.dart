class User {
  String id;
  String userName;
  String email;
  String password;

  User({this.id, this.userName, this.email, this.password});
}
