import 'package:meta/meta.dart';

/// PopResult
class PageInformation<T> {
  /// poped from this page
  String fromPage;

  /// current Page
  String currentPage;

  /// pop until this page
  String toPage;

  /// data to pass between page
  Map<String, T> results;

  /// constructor
  PageInformation(
      {@required this.fromPage,
      @required this.currentPage,
      this.toPage,
      this.results});
}
