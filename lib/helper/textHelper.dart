class TextHelper {
  static String capitalize(String text) {
    text = removeUnderscore(text);
    return "${text[0].toUpperCase()}${text.substring(1)}";
  }

  static String removeUnderscore(String text) =>
      text.replaceAll(RegExp('_'), ' ');

  static String removeSubInfo(String text) => (text.split("\/"))[0];

  static String newlineThethirdItem(String text) {
    List<String> list = text.split(",");
    list[2] = "\n" + list[2];
    return list.join(",");
  }
}
