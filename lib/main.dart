import 'package:flutter/material.dart';
import 'package:fyp_hiwithsign/providers/cameraProvider.dart';
// import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:fyp_hiwithsign/providers/feedbackProvider.dart';
import 'package:fyp_hiwithsign/providers/languageProvider.dart';
import 'package:fyp_hiwithsign/providers/learnProvider.dart';
import 'package:fyp_hiwithsign/providers/userProvider.dart';
import 'package:fyp_hiwithsign/screens/homeScreen/homeScreen.dart';
import 'package:fyp_hiwithsign/screens/learningScreens/learnScreen.dart';
import 'package:fyp_hiwithsign/screens/learningScreens/learnScreen_category.dart';
import 'package:fyp_hiwithsign/screens/feedbackScreen/feedBackScreen.dart';
import 'package:fyp_hiwithsign/screens/feedbackScreen/loginOrRegister.dart';
import 'package:fyp_hiwithsign/screens/learningScreens/learnScreen_categoryItems.dart';
import 'package:fyp_hiwithsign/screens/loginAndRegisterScreen/loginScreen.dart';
import 'package:fyp_hiwithsign/screens/loginAndRegisterScreen/registerScreen.dart';
import 'package:fyp_hiwithsign/screens/translateScreen/translateScreen.dart';
import 'package:fyp_hiwithsign/widgets/CustomAppBar.dart';
import 'package:provider/provider.dart';
import 'package:fyp_hiwithsign/screens/landingScreen/landingScreen.dart';
import 'package:fyp_hiwithsign/utils/globals.dart' as globals;
// import 'package:flutter_gen/gen_l10n/app_localizations.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // static void setLocale(BuildContext context, Locale newLocale) {
  //   _MyAppState state = context.findAncestorStateOfType<_MyAppState>();
  //   state.setLocale(newLocale);
  // }

  // static _MyAppState of(BuildContext context) =>
  //     context.findAncestorStateOfType<_MyAppState>();

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  // Locale _locale;
  // void setLocale(Locale locale) {
  //   setLocaleInStorage(locale.languageCode);
  //   setState(() {
  //     this._locale = locale;
  //   });
  // }

  // @override
  // void didChangeDependencies() {
  //   getLocaleInStorage().then((locale) {
  //     setState(() {
  //       this._locale = locale;
  //     });
  //   });
  //   super.didChangeDependencies();
  // }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<UserProvider>(
            create: (context) => UserProvider()),
        ChangeNotifierProvider<LearnProvider>(
            create: (context) => LearnProvider()),
        ChangeNotifierProvider<FeedbackProvider>(
            create: (context) => FeedbackProvider()),
        ChangeNotifierProvider<LanguageProvider>(
            create: (context) => LanguageProvider()),
        ChangeNotifierProvider<CameraProvider>(
            create: (context) => CameraProvider()),
      ],
      child: Builder(
        builder: (BuildContext context) {
          return new MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'HiWithSign',
            // locale: _locale,
            theme: ThemeData(
              primarySwatch: globals.primaryColour,
              scaffoldBackgroundColor: globals.primaryColour,
              floatingActionButtonTheme: FloatingActionButtonThemeData(
                backgroundColor: globals.primaryButtonColour,
                foregroundColor: globals.textColorWhite,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0),
                ),
              ),
              elevatedButtonTheme: ElevatedButtonThemeData(
                style: ButtonStyle(
                    foregroundColor: MaterialStateProperty.all<Color>(
                        globals.textColorWhite),
                    backgroundColor: MaterialStateProperty.all<Color>(
                        globals.primaryButtonColour),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(40.0),
                            side: BorderSide(color: globals.textColorWhite)))),
              ),
              textTheme: TextTheme(
                button: globals.buttonTextStyle,
              ),
            ),
            home: LandingScreen(),
            routes: {
              LandingScreen.routeName: (context) => LandingScreen(),
              HomeScreen.routeName: (context) => HomeScreen(),
              TranslateScreen.routeName: (context) => TranslateScreen([]),
              LearnScreenCategory.routeName: (context) => LearnScreenCategory(),
              LearnScreenItems.routeName: (context) =>
                  LearnScreenItems(extraRouteName: " "),
              LearnScreen.routeName: (context) => LearnScreen(
                    extraRouteName: " ",
                    item: null,
                  ),
              FeedbackScreen.routeName: (context) => FeedbackScreen(),
              LoginOrRegister.routeName: (context) => LoginOrRegister(),
              RegisterScreen.routeName: (context) => RegisterScreen(),
              LoginScreen.routeName: (context) => LoginScreen(),
              CustomAppBar.routeName: (context) => CustomAppBar(),
            },
            // supportedLocales: AppLocalizations.supportedLocales,
            // localizationsDelegates: AppLocalizations.localizationsDelegates,
            // [
            //   AppLocalizations.delegate,
            //   GlobalMaterialLocalizations.delegate,
            //   GlobalWidgetsLocalizations.delegate,
            //   GlobalCupertinoLocalizations.delegate,
            // ],
            // localeResolutionCallback: (locale, supportedLocales) {
            //   for (var supportedLocale in supportedLocales) {
            //     if (supportedLocale.languageCode == locale.languageCode &&
            //         supportedLocale.countryCode == locale.countryCode) {
            //       return supportedLocale;
            //     }
            //   }
            //   return supportedLocales.first;
            // },
            // localeResolutionCallback: (
            //   Locale locale,
            //   Iterable<Locale> supportedLocales,
            // ) {
            //   return locale;
            // },
          );
        },
      ),
    );
  }
}
