import 'package:camera/camera.dart';
import 'package:flutter/material.dart';

class CameraProvider extends ChangeNotifier {
  List<CameraDescription> _cameras;

  get cameras => _cameras ?? [];

  Future<void> init() async {
    // initialize the cameras when the app starts
    WidgetsFlutterBinding.ensureInitialized();
    _cameras = await availableCameras();
  }

  //TODO: add swtich camera
}
