import 'package:flutter/material.dart';
import 'package:fyp_hiwithsign/classes/connection_related/serverResponse.dart';
import 'package:fyp_hiwithsign/classes/model/user.dart';
import 'package:fyp_hiwithsign/services/feedbackService.dart';

class FeedbackProvider extends ChangeNotifier {
  Future<bool> submitFeedback(User user, String message) async {
    FeedbackService feedbackService = FeedbackService();
    ServerResponse serverRes =
        await feedbackService.submitFeedback(user, message);
    notifyListeners();
    return serverRes.success;
  }
}
