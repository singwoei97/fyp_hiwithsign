import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/services.dart';
import 'dart:async';
import 'dart:convert';

const String LAGUAGE_CODE = 'languageCode';

class LanguageProvider extends ChangeNotifier {
  Locale _currentLocale;
  Map<String, String> _localizedValues = {};

  Future<void> init() async {
    _currentLocale = await _getLocaleInStorage();
    load();
  }

  Future<void> setLocale(String languageCode) async {
    if (_currentLocale.languageCode != languageCode) {
      _setLocaleInStorage(languageCode);
      _currentLocale = _decideLocale(languageCode);
      load();
    }
    notifyListeners();
  }

  Locale getLocale(String languageCode) {
    return _currentLocale;
  }

  String s(String key) {
    return _localizedValues[key] ?? '';
  }

  Future<Locale> _setLocaleInStorage(String languageCode) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    await _prefs.setString(LAGUAGE_CODE, languageCode);
    return _decideLocale(languageCode);
  }

  Future<Locale> _getLocaleInStorage() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    String languageCode = _prefs.getString(LAGUAGE_CODE) ?? 'en';
    return _decideLocale(languageCode);
  }

  Locale _decideLocale(String languageCode) {
    switch (languageCode) {
      case 'en':
        return Locale('en');
      case 'bm':
        return Locale('bm');
      default:
        return Locale('en');
    }
  }

  Future<void> load() async {
    String jsonStringValues = await rootBundle
        .loadString('lib/lang/${_currentLocale.languageCode ?? 'en'}.json');
    Map<String, dynamic> mappedJson = json.decode(jsonStringValues);
    _localizedValues =
        mappedJson.map((key, value) => MapEntry(key, value.toString()));
    notifyListeners();
  }
}
