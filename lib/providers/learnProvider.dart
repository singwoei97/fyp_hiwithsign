import 'package:flutter/material.dart';
import 'package:fyp_hiwithsign/classes/model/category.dart';
import 'package:fyp_hiwithsign/classes/model/categoryItem.dart';
import 'package:fyp_hiwithsign/classes/model/sign.dart';
import 'package:fyp_hiwithsign/classes/model/video.dart';
import 'package:fyp_hiwithsign/services/learnService.dart';

class LearnProvider extends ChangeNotifier {
  List<Category> _categories;
  List<CategoryItem> _categoryItems;

  List<Category> get categories => _categories ?? [];

  List<CategoryItem> get categoryItems => _categoryItems ?? [];

  Future<void> getCategories() async {
    LearnService learnService = LearnService();
    _categories = await learnService.getCategory();
  }

  Future<void> getSign(String key) async {
    LearnService learnService = LearnService();
    _categoryItems = await learnService.getCategoryItems(key);
  }

  Future<Sign> getLearningVideo(
      String categoryKey, String signDocumentKey) async {
    LearnService learnService = LearnService();
    var sign = await learnService.getLearnVideo(categoryKey, signDocumentKey);
    return sign;
  }

  Future<Video> getTutorialVideo(String key) async {
    LearnService learnService = LearnService();
    var video = await learnService.getTutorialVideo(key);
    return video;
  }
}
