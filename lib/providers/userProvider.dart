import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fyp_hiwithsign/classes/connection_related/serverResponse.dart';
import 'package:fyp_hiwithsign/classes/model/user.dart';
import 'package:fyp_hiwithsign/services/auth.dart';

class UserProvider extends ChangeNotifier {
  User user;

  handleUserDataFromFirebase(FirebaseUser firebaseUser) {
    user = User(
        id: firebaseUser.uid,
        email: firebaseUser.email,
        userName: firebaseUser.displayName);
  }

  Future<bool> firebaseLogin(String email, String password) async {
    AuthService authService = AuthService();
    ServerResponse serverRes =
        await authService.signInWithEmailAndPassword(email, password);
    handleUserDataFromFirebase(serverRes.data);
    notifyListeners();
    return serverRes.success;
  }

  Future<bool> firebaseRegister(
      String email, String password, String userName) async {
    AuthService authService = AuthService();
    ServerResponse serverRes = await authService.registerWithEmailAndPassword(
        email, password, userName);

    handleUserDataFromFirebase(serverRes.data);
    notifyListeners();
    return serverRes.success;
  }

  Future<bool> firebaseLogout() async {
    AuthService authService = AuthService();
    ServerResponse serverRes = await authService.signOut();
    if (serverRes.success) user = null;
    notifyListeners();
    return serverRes.success;
  }
}
