import 'package:flutter/material.dart';
import 'package:fyp_hiwithsign/classes/screen_related/pageInformation.dart';
import 'package:fyp_hiwithsign/classes/screen_related/sizeInformation.dart';

class ScreenA extends StatelessWidget {
  final Widget Function(BuildContext context, SizeInformation sizeInfo) builder;
  final PageInformation pageInformation;

  ScreenA({
    @required this.builder,
    @required this.pageInformation,
  });

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    var orientation = MediaQuery.of(context).orientation;

    SizeInformation sizeInfo = SizeInformation(width, height, orientation);

    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Builder(
          builder: (context) {
            return builder(context, sizeInfo);
          },
        ),
      ),
    );
  }
}
