import 'package:flutter/material.dart';
import 'package:fyp_hiwithsign/classes/screen_related/pageInformation.dart';
import 'package:fyp_hiwithsign/classes/screen_related/sizeInformation.dart';

class ScreenB extends StatefulWidget {
  static String routeName = "ScreenB_forHomeScreen";
  final Widget Function(BuildContext context, SizeInformation sizeInfo) builder;
  final PageInformation pageInformation;
  final Widget appBar;

  ScreenB({
    @required this.builder,
    @required this.pageInformation,
    this.appBar,
  });

  @override
  _ScreenBState createState() => _ScreenBState();
}

class _ScreenBState extends State<ScreenB> {
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    var orientation = MediaQuery.of(context).orientation;

    SizeInformation sizeInfo = SizeInformation(width, height, orientation);

    return SafeArea(
      child: Scaffold(
        appBar: widget.appBar,
        resizeToAvoidBottomInset: false,
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 10.0),
          child: Builder(
            builder: (context) {
              return widget.builder(context, sizeInfo);
            },
          ),
        ),
      ),
    );
  }
}
