import 'package:flutter/material.dart';
import 'package:fyp_hiwithsign/classes/screen_related/pageInformation.dart';
import 'package:fyp_hiwithsign/classes/screen_related/sizeInformation.dart';
import 'package:fyp_hiwithsign/providers/languageProvider.dart';
//import 'package:fyp_hiwithsign/utils/language_constant.dart';
import 'package:fyp_hiwithsign/widgets/CustomAppBar.dart';
import 'package:provider/provider.dart';
// import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ScreenD extends StatelessWidget {
  final Widget Function(BuildContext context, SizeInformation sizeInfo) builder;
  final PageInformation pageInformation;
  final Function backButtonOnPressed;
  final Widget customSettingButton;
  final Function customSettingFunction;
  final Color extraDescriptionBackground;
  final Text extraDescription;
  final bool settingButtnOn;

  ScreenD(
      {@required this.builder,
      @required this.pageInformation,
      @required this.backButtonOnPressed,
      this.settingButtnOn = false,
      this.customSettingButton,
      this.customSettingFunction,
      this.extraDescriptionBackground,
      this.extraDescription});

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    var orientation = MediaQuery.of(context).orientation;
    var languageProvider = Provider.of<LanguageProvider>(context);

    SizeInformation sizeInfo = SizeInformation(width, height, orientation);
    bool keyboardIsOpened = MediaQuery.of(context).viewInsets.bottom != 0.0;
    return SafeArea(
      child: Scaffold(
        appBar: CustomAppBar(
          pageInformation: pageInformation,
          settingButtonOn: settingButtnOn,
          settingButtonCustomFunction: customSettingFunction,
          customSettingButton: customSettingButton,
          extraDescription: extraDescription,
          extraDescriptionBackground: extraDescriptionBackground,
        ),
        resizeToAvoidBottomPadding: false,
        resizeToAvoidBottomInset: true,
        body: SingleChildScrollView(
          child: Builder(
            builder: (context) {
              return builder(context, sizeInfo);
            },
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.startFloat,
        floatingActionButton: Visibility(
          visible: !keyboardIsOpened ? true : false,
          child: FloatingActionButton.extended(
            onPressed: backButtonOnPressed ??
                () {
                  print("Back button pressed.");
                },
            label: Text(languageProvider.s('back')),
          ),
        ),
      ),
    );
  }
}
