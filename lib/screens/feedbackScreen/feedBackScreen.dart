import 'package:flutter/material.dart';
import 'package:fyp_hiwithsign/classes/screen_related/pageInformation.dart';
import 'package:fyp_hiwithsign/classes/screen_related/sizeInformation.dart';
import 'package:fyp_hiwithsign/providers/feedbackProvider.dart';
import 'package:fyp_hiwithsign/providers/languageProvider.dart';
import 'package:fyp_hiwithsign/providers/userProvider.dart';
import 'package:fyp_hiwithsign/screen_template/screen_type_D.dart';
import 'package:fyp_hiwithsign/screens/homeScreen/homeScreen.dart';
import 'package:fyp_hiwithsign/widgets/alertDialog.dart';
import 'package:fyp_hiwithsign/widgets/textFormFieldCustom.dart';
import 'package:provider/provider.dart';
import 'package:fyp_hiwithsign/utils/globals.dart' as globals;

class FeedbackScreen extends StatefulWidget {
  static String routeName = "Feedback";
  final PageInformation pageInfo;

  FeedbackScreen({Key key, this.pageInfo}) : super(key: key);

  @override
  _FeedbackScreenState createState() => _FeedbackScreenState();
}

class _FeedbackScreenState extends State<FeedbackScreen> {
  FeedbackProvider feedbackProvider;
  UserProvider userProvider;
  PageInformation pageInfo;
  final GlobalKey<FormState> _feedbackFormKey = new GlobalKey<FormState>();
  TextEditingController _feddbackMessageController = TextEditingController();

  @override
  void initState() {
    pageInfo = PageInformation(
        fromPage: widget.pageInfo.currentPage,
        currentPage: widget.pageInfo.toPage);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    feedbackProvider = Provider.of<FeedbackProvider>(context);
    userProvider = Provider.of<UserProvider>(context);
    var languageProvider = Provider.of<LanguageProvider>(context);
    return ScreenD(
      builder: (BuildContext context, SizeInformation sizeInfo) {
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: Center(
            child: Column(
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    languageProvider.s('tell_us'),
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 22.0,
                      fontFamily: 'RozhaOne',
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Form(
                  key: _feedbackFormKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TextFormFieldCustom(
                        _feddbackMessageController,
                        maxlines: 14,
                        textInputAction: TextInputAction.newline,
                        hintText: "Type here...",
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all<Color>(
                        globals.buttonColours[2]),
                  ),
                  onPressed: () async {
                    alertDialog(context, dialogBoxType: DialogBoxType.loading);
                    bool success = await feedbackProvider.submitFeedback(
                        userProvider.user, _feddbackMessageController.text);
                    Navigator.of(context).pop(); // Pop alert dialog
                    alertDialog(
                      context,
                      title: FeedbackScreen.routeName,
                      content: success
                          ? languageProvider.s('submitted')
                          : languageProvider.s('failed'),
                    );
                    if (success) _feddbackMessageController.clear();
                  },
                  child: Text(languageProvider.s('submit')),
                ),
                SizedBox(
                  height: 20,
                ),
              ],
            ),
          ),
        );
      },
      settingButtnOn: true,
      pageInformation: pageInfo,
      extraDescription: Text(
        "Hi, ${userProvider.user?.userName ?? "John Doe"}",
        style: TextStyle(fontSize: 22.0, fontFamily: 'RozhaOne'),
      ),
      customSettingButton: CircleAvatar(
        backgroundColor: globals.settingButtonColour,
        child: IconButton(
          icon: Icon(
            Icons.logout,
            color: Colors.white,
          ),
          onPressed: () async {
            bool loggedOut = await logout();
            if (loggedOut) {
              pageInfo.toPage = HomeScreen.routeName;
              Navigator.of(context).popUntil((route) {
                return route.settings.name == pageInfo.toPage;
              });
            }
          },
        ),
      ),
      backButtonOnPressed: () {
        Navigator.pop(context);
      },
    );
  }

  Future<bool> logout() async {
    Future<bool> serverRes =
        Provider.of<UserProvider>(context, listen: false).firebaseLogout();
    return serverRes;
  }
}
