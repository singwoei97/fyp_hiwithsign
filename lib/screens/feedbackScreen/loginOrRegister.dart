import 'package:flutter/material.dart';
import 'package:fyp_hiwithsign/classes/screen_related/pageInformation.dart';
import 'package:fyp_hiwithsign/classes/screen_related/sizeInformation.dart';
import 'package:fyp_hiwithsign/providers/languageProvider.dart';
import 'package:fyp_hiwithsign/screen_template/screen_type_D.dart';
import 'package:fyp_hiwithsign/screens/loginAndRegisterScreen/loginScreen.dart';
import 'package:fyp_hiwithsign/screens/loginAndRegisterScreen/registerScreen.dart';
import 'package:fyp_hiwithsign/utils/globals.dart' as globals;
import 'package:provider/provider.dart';

class LoginOrRegister extends StatefulWidget {
  static String routeName = "Login Or Register";
  final PageInformation pageInfo;

  LoginOrRegister({Key key, this.pageInfo}) : super(key: key);

  @override
  _LoginOrRegisterState createState() => _LoginOrRegisterState();
}

class _LoginOrRegisterState extends State<LoginOrRegister> {
  PageInformation pageInfo;

  @override
  void initState() {
    pageInfo = PageInformation(
        fromPage: widget.pageInfo.currentPage,
        currentPage: widget.pageInfo.toPage);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var languageProvider = Provider.of<LanguageProvider>(context);
    return ScreenD(
      builder: (BuildContext context, SizeInformation sizeInfo) {
        return Center(
          child: Column(
            children: [
              SizedBox(
                height: 50,
              ),
              Text(
                languageProvider.s('feedback_instruction'),
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 30.0,
                  fontFamily: 'RozhaOne',
                ),
              ),
              SizedBox(
                height: 50,
              ),
              Column(
                children: [
                  ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all<Color>(
                          globals.buttonColours[0]),
                    ),
                    onPressed: () {
                      pageInfo.toPage = LoginScreen.routeName;
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              settings: RouteSettings(name: pageInfo.toPage),
                              builder: (context) {
                                return LoginScreen(pageInfo: pageInfo);
                              }));
                    },
                    child: Text(languageProvider.s('login')),
                  ),
                  SizedBox(
                    height: 80,
                  ),
                  ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all<Color>(
                          globals.buttonColours[0]),
                    ),
                    onPressed: () {
                      pageInfo.toPage = RegisterScreen.routeName;
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              settings: RouteSettings(name: pageInfo.toPage),
                              builder: (context) {
                                return RegisterScreen(pageInfo: pageInfo);
                              }));
                    },
                    child: Text(languageProvider.s('register')),
                  ),
                ],
              ),
            ],
          ),
        );
      },
      pageInformation: pageInfo,
      backButtonOnPressed: () {
        Navigator.pop(context);
      },
    );
  }
}
