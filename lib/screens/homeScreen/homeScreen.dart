import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fyp_hiwithsign/classes/model/video.dart';
import 'package:fyp_hiwithsign/classes/screen_related/pageInformation.dart';
import 'package:fyp_hiwithsign/providers/cameraProvider.dart';
import 'package:fyp_hiwithsign/providers/languageProvider.dart';
import 'package:fyp_hiwithsign/providers/learnProvider.dart';
// import 'package:fyp_hiwithsign/classes/screen_related/sizeInformation.dart';
import 'package:fyp_hiwithsign/providers/userProvider.dart';
import 'package:fyp_hiwithsign/screen_template/screen_type_B.dart';
import 'package:fyp_hiwithsign/screens/feedbackScreen/feedBackScreen.dart';
import 'package:fyp_hiwithsign/screens/feedbackScreen/loginOrRegister.dart';
import 'package:fyp_hiwithsign/screens/learningScreens/learnScreen_category.dart';
import 'package:fyp_hiwithsign/screens/translateScreen/translateScreen.dart';
import 'package:fyp_hiwithsign/utils/globals.dart' as globals;
import 'package:fyp_hiwithsign/widgets/CustomAppBar.dart';
import 'package:fyp_hiwithsign/widgets/settingDialog.dart';
// import 'package:fyp_hiwithsign/utils/language_constant.dart';
import 'package:provider/provider.dart';
import 'package:video_player/video_player.dart';
// import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class HomeScreen extends StatefulWidget {
  static String routeName = "HiWithSign";
  final PageInformation pageInfo;

  HomeScreen({this.pageInfo});

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  PageInformation pageInfo;
  List<String> mainMenuList;
  VideoPlayerController controller;

  @override
  void initState() {
    pageInfo = PageInformation(
        fromPage: widget.pageInfo.currentPage,
        currentPage: widget.pageInfo.toPage);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var languageProvider = Provider.of<LanguageProvider>(context);
    mainMenuList = [
      languageProvider.s('translate'),
      languageProvider.s('learn'),
      languageProvider.s('feedback'),
      languageProvider.s('exit')
    ];
    return ScreenB(
        appBar: CustomAppBar(
          pageInformation: pageInfo,
          settingButtonOn: true,
          settingButtonCustomFunction:
              showDialogBox(languageProvider.s('language_selection')),
        ),
        builder: (context, sizeInfo) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Stack(
                alignment: AlignmentDirectional.bottomStart,
                children: [
                  Align(
                    alignment: Alignment.centerRight,
                    child: SizedBox(
                      width: 300,
                      height: 300,
                      child: Image.asset(
                        'assets/images/sign-language.png',
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomLeft,
                    child: Row(
                      children: [
                        ElevatedButton(
                          style: ButtonStyle(
                            padding: MaterialStateProperty.all(
                              EdgeInsets.symmetric(
                                  vertical: 10, horizontal: 20),
                            ),
                            backgroundColor: MaterialStateProperty.all<Color>(
                                globals.buttonColours[0]),
                          ),
                          onPressed: () {
                            var cameras = Provider.of<CameraProvider>(context,
                                    listen: false)
                                .cameras;
                            pageInfo.toPage = TranslateScreen.routeName;
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  settings:
                                      RouteSettings(name: pageInfo.toPage),
                                  builder: (context) {
                                    return TranslateScreen(cameras,
                                        pageInfo: pageInfo);
                                  }),
                            );
                          },
                          child: Text(mainMenuList[0]),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 5),
                          child: CircleAvatar(
                            radius: 20,
                            backgroundColor: Colors.white,
                            child: IconButton(
                                color: Colors.black,
                                icon: Icon(Icons.contact_support),
                                onPressed: () async {
                                  controller = await prepareController(
                                      "translateTutorial");
                                  videoDialog(controller).then((value) {
                                    print("exit dialog");
                                  });
                                }),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Expanded(
                child: Stack(
                  alignment: AlignmentDirectional.topStart,
                  children: [
                    Positioned(
                      left: 20,
                      child: Container(
                        margin: EdgeInsets.all(20),
                        child: Row(
                          children: [
                            ElevatedButton(
                              style: ButtonStyle(
                                padding: MaterialStateProperty.all(
                                    EdgeInsets.symmetric(
                                        vertical: 10, horizontal: 40)),
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(
                                        globals.buttonColours[1]),
                              ),
                              onPressed: () {
                                pageInfo.toPage = LearnScreenCategory.routeName;
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    settings:
                                        RouteSettings(name: pageInfo.toPage),
                                    builder: (context) {
                                      return LearnScreenCategory(
                                          pageInfo: pageInfo);
                                    },
                                  ),
                                );
                              },
                              child: Text(mainMenuList[1]),
                            ),
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 5),
                              child: CircleAvatar(
                                radius: 20,
                                backgroundColor: Colors.white,
                                child: IconButton(
                                  color: Colors.black,
                                  icon: Icon(Icons.contact_support),
                                  onPressed: () async {
                                    controller = await prepareController(
                                        "learnTutorial");
                                    videoDialog(controller).then((value) {
                                      print("exit dialog");
                                    });
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Positioned(
                      top: 80,
                      left: 40,
                      child: Container(
                        margin: EdgeInsets.all(20),
                        child: Row(
                          children: [
                            ElevatedButton(
                              style: ButtonStyle(
                                padding: MaterialStateProperty.all(
                                    EdgeInsets.symmetric(
                                        vertical: 10, horizontal: 20)),
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(
                                        globals.buttonColours[2]),
                              ),
                              onPressed: () async {
                                bool userLoggedIn = await checkUserLoggedIn();
                                if (userLoggedIn) {
                                  pageInfo.toPage = FeedbackScreen.routeName;
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      settings:
                                          RouteSettings(name: pageInfo.toPage),
                                      builder: (context) {
                                        return FeedbackScreen(
                                            pageInfo: pageInfo);
                                      },
                                    ),
                                  );
                                } else {
                                  pageInfo.toPage = LoginOrRegister.routeName;
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      settings:
                                          RouteSettings(name: pageInfo.toPage),
                                      builder: (context) {
                                        return LoginOrRegister(
                                            pageInfo: pageInfo);
                                      },
                                    ),
                                  );
                                }
                              },
                              child: Text(mainMenuList[2]),
                            ),
                            // Feedback button
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 5),
                              child: CircleAvatar(
                                radius: 20,
                                backgroundColor: Colors.white,
                                child: IconButton(
                                  color: Colors.black,
                                  icon: Icon(Icons.contact_support),
                                  onPressed: () async {
                                    controller = await prepareController(
                                        "feedbackTutorial");
                                    videoDialog(controller).then((value) {
                                      print("exit dialog");
                                    });
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    // Exit button
                    Positioned(
                      top: 160,
                      left: 150,
                      child: Container(
                        margin: EdgeInsets.all(20),
                        child: ElevatedButton(
                            style: ButtonStyle(
                              padding: MaterialStateProperty.all(
                                  EdgeInsets.symmetric(
                                      vertical: 10, horizontal: 20)),
                              foregroundColor: MaterialStateProperty.all<Color>(
                                  globals.textColorWhite),
                              backgroundColor: MaterialStateProperty.all<Color>(
                                  globals.buttonColours[3]),
                            ),
                            onPressed: () {
                              SystemNavigator.pop();
                            },
                            child: Text(mainMenuList[3])),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          );
        },
        pageInformation: pageInfo);
  }

  Future<bool> checkUserLoggedIn() async {
    return Provider.of<UserProvider>(context, listen: false).user == null
        ? false
        : true;
  }

  Function showDialogBox(String text) {
    return () {
      // print("nani");
      // Scaffold.of(context).showSnackBar(SnackBar(
      //   content: Text("naini"),
      // ));
      showDialog(
        context: context,
        child: SettingDialog(alertText: text),
      );
    };
  }

  Future<VideoPlayerController> prepareController(String tutorialKey) async {
    Video video = await Provider.of<LearnProvider>(
      context,
      listen: false,
    ).getTutorialVideo(tutorialKey);
    VideoPlayerController controller = VideoPlayerController.network(
      video.videoPath,
    )..initialize();
    return controller;
  }

  Future<bool> videoDialog(VideoPlayerController _controller,
      {String tutorialKey}) async {
    print("i am in");
    _controller.play();
    return showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) => AlertDialog(
            elevation: 10,
            title: Text("Tutorial"),
            content: VideoPlayer(_controller),
            actions: <Widget>[
              ElevatedButton(
                onPressed: () async {
                  Navigator.of(context).pop(true);
                },
                child: Text("Ok"),
              ),
            ],
          ),
        ) ??
        false;
  }
}
