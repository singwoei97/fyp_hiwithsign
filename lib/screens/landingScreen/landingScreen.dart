import 'package:flutter/material.dart';
import 'package:fyp_hiwithsign/classes/screen_related/pageInformation.dart';
import 'package:fyp_hiwithsign/classes/screen_related/sizeInformation.dart';
import 'package:fyp_hiwithsign/providers/cameraProvider.dart';
import 'package:fyp_hiwithsign/providers/languageProvider.dart';
import 'package:fyp_hiwithsign/providers/learnProvider.dart';
import 'package:fyp_hiwithsign/screen_template/screen_type_A.dart';
import 'package:fyp_hiwithsign/screens/homeScreen/homeScreen.dart';
// import 'package:fyp_hiwithsign/utils/language_constant.dart';
// import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

class LandingScreen extends StatefulWidget {
  static String routeName = "landing";

  @override
  _LandingScreenState createState() => _LandingScreenState();
}

class _LandingScreenState extends State<LandingScreen> {
  PageInformation pageInfo;

  @override
  void initState() {
    pageInfo = PageInformation(
        fromPage: LandingScreen.routeName,
        currentPage: LandingScreen.routeName);
    // Load up language provider
    Provider.of<LanguageProvider>(context, listen: false).init();
    // Load up cameras
    Provider.of<CameraProvider>(context, listen: false).init();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var languageProvider = Provider.of<LanguageProvider>(context);
    return ScreenA(
      builder: (BuildContext context, SizeInformation sizeInfo) {
        return Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Column(
                children: [
                  SizedBox(height: 20),
                  CircleAvatar(
                      child: Image.asset('assets/images/sign-language.png'),
                      radius: 100.0),
                  SizedBox(height: 20),
                  Text(
                    languageProvider.s('app_decription'),
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 36, fontFamily: 'RozhaOne'),
                  ),
                ],
              ),
              ElevatedButton(
                onPressed: () {
                  pageInfo.toPage = HomeScreen.routeName;
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          settings: RouteSettings(name: pageInfo.toPage),
                          builder: (context) {
                            return HomeScreen(pageInfo: pageInfo);
                          }));
                },
                child: Text(
                  languageProvider.s('next'),
                ),
              ),
            ],
          ),
        );
      },
      pageInformation: pageInfo,
    );
  }
}
