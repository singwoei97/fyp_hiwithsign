import 'package:flutter/material.dart';
import 'package:fyp_hiwithsign/classes/model/categoryItem.dart';
import 'package:fyp_hiwithsign/classes/model/sign.dart';
import 'package:fyp_hiwithsign/classes/model/video.dart';
import 'package:fyp_hiwithsign/classes/screen_related/pageInformation.dart';
import 'package:fyp_hiwithsign/classes/screen_related/sizeInformation.dart';
import 'package:fyp_hiwithsign/providers/languageProvider.dart';
import 'package:fyp_hiwithsign/providers/learnProvider.dart';
import 'package:fyp_hiwithsign/screen_template/screen_type_D.dart';
import 'package:fyp_hiwithsign/utils/globals.dart' as globals;
import 'package:fyp_hiwithsign/widgets/borderLineText.dart';
import 'package:provider/provider.dart';
import 'package:video_player/video_player.dart';

class LearnScreen extends StatefulWidget {
  static String routeName = "Learn To Sign/Video";
  final String extraRouteName;
  final CategoryItem item;
  final PageInformation pageInfo;

  LearnScreen(
      {Key key,
      this.pageInfo,
      @required this.extraRouteName,
      @required this.item})
      : super(key: key);

  @override
  _LearnScreenState createState() => _LearnScreenState();
}

class _LearnScreenState extends State<LearnScreen> {
  PageInformation pageInfo;
  VideoPlayerController _videoPlayerController;

  @override
  void initState() {
    pageInfo = PageInformation(
        fromPage: widget.pageInfo.currentPage,
        currentPage: widget.pageInfo.toPage);
    super.initState();
    prepareController(widget.extraRouteName, widget.item.itemID);
  }

  @override
  void dispose() {
    super.dispose();
    _videoPlayerController?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var languageProvider = Provider.of<LanguageProvider>(context);
    return ScreenD(
      builder: (BuildContext context, SizeInformation sizeInfo) {
        return Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              _videoPlayerController?.value?.initialized ?? false
                  ? SizedBox(
                      height: 330,
                      width: 230,
                      child: VideoPlayer(_videoPlayerController),
                    )
                  : SizedBox(
                      height: 330,
                      width: 230,
                      child: Container(
                        color: Colors.black,
                        child: Center(
                          child: CircularProgressIndicator(),
                        ),
                      ),
                    ),
              Container(
                child: BorderText(widget.item.itemName, fontSize: 50),
              ),
              ElevatedButton(
                style: ButtonStyle(
                  padding: MaterialStateProperty.all(
                      EdgeInsets.symmetric(vertical: 5, horizontal: 20)),
                  backgroundColor: MaterialStateProperty.all<Color>(
                      globals.buttonColours[2]),
                ),
                onPressed: () async {
                  if (_videoPlayerController != null)
                    setState(() {
                      _videoPlayerController?.value?.isPlaying ?? false
                          ? _videoPlayerController.pause()
                          : _videoPlayerController.play();
                    });
                },
                child: Text(_videoPlayerController?.value?.isPlaying ?? false
                    ? languageProvider.s('pause')
                    : languageProvider.s('play')),
              ),
              SizedBox(
                height: 50,
              ),
            ],
          ),
        );
      },
      settingButtnOn: true,
      pageInformation: pageInfo,
      extraDescription: Text(
        widget.extraRouteName,
        style: TextStyle(
          fontSize: 22.0,
          fontFamily: 'RozhaOne',
          color: globals.textColorWhite,
        ),
      ),
      extraDescriptionBackground: globals.buttonColours[0],
      customSettingButton: CircleAvatar(
        backgroundColor: globals.buttonColours[2],
      ),
      backButtonOnPressed: () {
        Navigator.pop(context);
      },
    );
  }

  Future<void> prepareController(
      String categoryKey, String signDocumentKey) async {
    Sign sign = await Provider.of<LearnProvider>(
      context,
      listen: false,
    ).getLearningVideo(categoryKey, signDocumentKey);
    if (sign.video.videoPath != "")
      _videoPlayerController = VideoPlayerController.network(
        sign.video.videoPath,
      )..initialize().then((value) => setState(() {}));
  }
}
