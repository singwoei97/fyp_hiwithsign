import 'package:flutter/material.dart';
import 'package:fyp_hiwithsign/classes/model/category.dart';
import 'package:fyp_hiwithsign/classes/screen_related/pageInformation.dart';
import 'package:fyp_hiwithsign/classes/screen_related/sizeInformation.dart';
import 'package:fyp_hiwithsign/providers/learnProvider.dart';
import 'package:fyp_hiwithsign/screen_template/screen_type_D.dart';
import 'package:fyp_hiwithsign/screens/learningScreens/learnScreen_categoryItems.dart';
import 'package:fyp_hiwithsign/widgets/learnCategoryListTile.dart';
import 'package:provider/provider.dart';

class LearnScreenCategory extends StatefulWidget {
  static String routeName = "Learn To Sign";
  final PageInformation pageInfo;

  LearnScreenCategory({Key key, this.pageInfo}) : super(key: key);

  @override
  _LearnScreenCategoryState createState() => _LearnScreenCategoryState();
}

class _LearnScreenCategoryState extends State<LearnScreenCategory> {
  PageInformation pageInfo;

  @override
  void initState() {
    pageInfo = PageInformation(
        fromPage: widget.pageInfo.currentPage,
        currentPage: widget.pageInfo.toPage);
    super.initState();
    Provider.of<LearnProvider>(context, listen: false).getCategories().then(
          (value) => setState(() {}),
        );
  }

  @override
  Widget build(BuildContext context) {
    List<Category> categories =
        Provider.of<LearnProvider>(context, listen: false).categories;
    return ScreenD(
      builder: (BuildContext context, SizeInformation sizeInfo) {
        return Column(
          children: [
            Container(
              child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                  physics: ScrollPhysics(),
                  itemCount: categories.length,
                  padding:
                      EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                      margin:
                          EdgeInsets.symmetric(vertical: 15, horizontal: 10),
                      child: LearnCategoryListTile(
                        index,
                        category: categories[index].categoryName,
                        categoryExample: categories[index].categoryExample,
                        onPressed: () async {
                          await Provider.of<LearnProvider>(context,
                                  listen: false)
                              .getSign(categories[index].categoryName);
                          pageInfo.toPage = LearnScreenItems.routeName;
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  settings:
                                      RouteSettings(name: pageInfo.toPage),
                                  builder: (context) {
                                    return LearnScreenItems(
                                      pageInfo: pageInfo,
                                      extraRouteName:
                                          categories[index].categoryName,
                                    );
                                  }));
                        },
                      ),
                    );
                  }),
            ),
            SizedBox(
              height: 50,
            ),
          ],
        );
      },
      pageInformation: pageInfo,
      backButtonOnPressed: () {
        Navigator.pop(context);
      },
    );
  }
}
