import 'package:flutter/material.dart';
import 'package:fyp_hiwithsign/classes/model/categoryItem.dart';
import 'package:fyp_hiwithsign/classes/screen_related/pageInformation.dart';
import 'package:fyp_hiwithsign/classes/screen_related/sizeInformation.dart';
import 'package:fyp_hiwithsign/providers/learnProvider.dart';
import 'package:fyp_hiwithsign/screen_template/screen_type_D.dart';
import 'package:fyp_hiwithsign/screens/learningScreens/learnScreen.dart';
import 'package:fyp_hiwithsign/utils/globals.dart' as globals;
import 'package:fyp_hiwithsign/widgets/learnItemsListTile.dart';
import 'package:provider/provider.dart';

class LearnScreenItems extends StatefulWidget {
  static String routeName = "Learn To Sign/Items";
  final String extraRouteName;
  final PageInformation pageInfo;

  LearnScreenItems({Key key, this.pageInfo, @required this.extraRouteName})
      : super(key: key);

  @override
  _LearnScreenItemsState createState() => _LearnScreenItemsState();
}

class _LearnScreenItemsState extends State<LearnScreenItems> {
  PageInformation pageInfo;

  @override
  void initState() {
    pageInfo = PageInformation(
        fromPage: widget.pageInfo.currentPage,
        currentPage: widget.pageInfo.toPage);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List<CategoryItem> categoryItems =
        Provider.of<LearnProvider>(context, listen: false).categoryItems;
    return ScreenD(
      builder: (BuildContext context, SizeInformation sizeInfo) {
        return Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
              child: ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                physics: ScrollPhysics(),
                itemCount: categoryItems.length,
                padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                itemBuilder: (BuildContext context, int index) {
                  return LearnItemsListTile(
                    index,
                    items: categoryItems[index].itemName,
                    onPressed: () {
                      pageInfo.toPage = LearnScreenItems.routeName;
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          settings: RouteSettings(name: pageInfo.toPage),
                          builder: (context) {
                            return LearnScreen(
                              pageInfo: pageInfo,
                              extraRouteName: widget.extraRouteName,
                              item: categoryItems[index],
                            );
                          },
                        ),
                      );
                    },
                  );
                },
              ),
            ),
            SizedBox(
              height: 50,
            ),
          ],
        );
      },
      settingButtnOn: true,
      pageInformation: pageInfo,
      extraDescription: Text(
        widget.extraRouteName,
        style: TextStyle(
            fontSize: 22.0,
            fontFamily: 'RozhaOne',
            color: globals.textColorWhite),
      ),
      extraDescriptionBackground: globals.buttonColours[0],
      customSettingButton: CircleAvatar(
        backgroundColor: globals.buttonColours[2],
      ),
      backButtonOnPressed: () {
        Navigator.pop(context);
      },
    );
  }
}
