import 'package:flutter/material.dart';
import 'package:fyp_hiwithsign/classes/screen_related/pageInformation.dart';
import 'package:fyp_hiwithsign/classes/screen_related/sizeInformation.dart';
import 'package:fyp_hiwithsign/providers/languageProvider.dart';
import 'package:fyp_hiwithsign/providers/userProvider.dart';
import 'package:fyp_hiwithsign/screen_template/screen_type_D.dart';
import 'package:fyp_hiwithsign/screens/feedbackScreen/feedBackScreen.dart';
import 'package:fyp_hiwithsign/screens/homeScreen/homeScreen.dart';
import 'package:fyp_hiwithsign/widgets/alertDialog.dart';
import 'package:fyp_hiwithsign/widgets/textFormFieldCustom.dart';
import 'package:fyp_hiwithsign/utils/globals.dart' as globals;
import 'package:provider/provider.dart';

class LoginScreen extends StatefulWidget {
  static String routeName = "Login";
  final PageInformation pageInfo;

  LoginScreen({Key key, this.pageInfo}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  PageInformation pageInfo;
  final GlobalKey<FormState> _loginFormKey = new GlobalKey<FormState>();
  TextEditingController _emailTxtController = TextEditingController();
  TextEditingController _passwordTxtController = TextEditingController();

  UserProvider userProvider;

  @override
  void initState() {
    pageInfo = PageInformation(
        fromPage: widget.pageInfo.currentPage,
        currentPage: widget.pageInfo.toPage);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    userProvider = Provider.of<UserProvider>(context);
    var languageProvider = Provider.of<LanguageProvider>(context);
    return ScreenD(
      builder: (BuildContext context, SizeInformation sizeInfo) {
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: Center(
            child: Column(
              children: [
                SizedBox(
                  height: 50,
                ),
                Text(
                  languageProvider.s('feedback_instruction'),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 22.0,
                    fontFamily: 'RozhaOne',
                  ),
                ),
                SizedBox(
                  height: 40,
                ),
                Form(
                  key: _loginFormKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                          child: Text(
                            languageProvider.s('email'),
                          ),
                          margin: EdgeInsets.symmetric(
                              horizontal: 10, vertical: 10)),
                      TextFormFieldCustom(
                        _emailTxtController,
                        hintText: "hiwithsign@mail.com",
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Container(
                          child: Text(
                            languageProvider.s('password'),
                          ),
                          margin: EdgeInsets.symmetric(
                              horizontal: 10, vertical: 10)),
                      TextFormFieldCustom(
                        _passwordTxtController,
                        hintText: "************",
                        obscureText: true,
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all<Color>(
                        globals.buttonColours[0]),
                  ),
                  onPressed: () async {
                    alertDialog(context, dialogBoxType: DialogBoxType.loading);
                    bool loggedin = await userProvider.firebaseLogin(
                        _emailTxtController.text, _passwordTxtController.text);
                    Navigator.of(context).pop(); // Pop alert dialog
                    if (loggedin) {
                      pageInfo.toPage = HomeScreen.routeName;
                      Navigator.of(context).popUntil((route) {
                        return route.settings.name == pageInfo.toPage;
                      });
                      pageInfo.toPage = FeedbackScreen.routeName;
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              settings: RouteSettings(name: pageInfo.toPage),
                              builder: (context) {
                                return FeedbackScreen(pageInfo: pageInfo);
                              }));
                    }
                  },
                  child: Text(
                    languageProvider.s('login'),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
              ],
            ),
          ),
        );
      },
      pageInformation: pageInfo,
      backButtonOnPressed: () {
        Navigator.pop(context);
      },
    );
  }
}
