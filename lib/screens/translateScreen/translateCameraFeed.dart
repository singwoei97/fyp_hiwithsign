import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'package:tflite/tflite.dart';
import 'dart:math' as math;

typedef void Callback(List<dynamic> list, int h, int w);

class TranslateCameraFeed extends StatefulWidget {
  final List<CameraDescription> cameras;
  final Callback setRecognitions;
  final Widget childLayout;
  // The TranslateCameraFeed Class takes the cameras list and the setRecognitions
  // function as argument
  TranslateCameraFeed(this.cameras, this.setRecognitions, this.childLayout);

  @override
  _TranslateCameraFeedState createState() => new _TranslateCameraFeedState();
}

class _TranslateCameraFeedState extends State<TranslateCameraFeed> {
  CameraController controller;
  bool isDetecting = false;
  // XFile videoFile;
  // bool recording = false;

  @override
  void initState() {
    super.initState();
    print(widget.cameras);
    if (widget.cameras == null || widget.cameras.length < 1) {
      print('No Cameras Found.');
    } else {
      controller = new CameraController(
        widget.cameras[0],
        ResolutionPreset.high,
      );
      controller.initialize().then((_) {
        if (!mounted) {
          return;
        }
        setState(() {});

        controller.startImageStream((CameraImage img) {
          if (!isDetecting) {
            isDetecting = true;
            Tflite.detectObjectOnFrame(
              bytesList: img.planes.map((plane) {
                return plane.bytes;
              }).toList(),
              model: "SSDMobileNet",
              imageHeight: img.height,
              imageWidth: img.width,
              imageMean: 127.5,
              imageStd: 127.5,
              numResultsPerClass: 1,
              threshold: 0.4,
            ).then((recognitions) {
              /*
              When setRecognitions is called here, the parameters are being passed on to the parent widget as callback. i.e. to the LiveFeed class
               */
              widget.setRecognitions(recognitions, img.height, img.width);
              isDetecting = false;
            });
          }
        });
      });
    }
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (controller == null || !controller.value.isInitialized) {
      return Container();
    }

    var tmp = MediaQuery.of(context).size;
    var screenH = math.max(tmp.height, tmp.width);
    var screenW = math.min(tmp.height, tmp.width);
    tmp = controller.value.previewSize;
    var previewH = math.max(tmp.height, tmp.width);
    var previewW = math.min(tmp.height, tmp.width);
    var screenRatio = screenH / screenW;
    var previewRatio = previewH / previewW;

    return OverflowBox(
      maxHeight:
          screenRatio > previewRatio ? screenH : screenW / previewW * previewH,
      maxWidth:
          screenRatio > previewRatio ? screenH / previewH * previewW : screenW,
      child: CameraPreview(
        controller,
        child: widget.childLayout,
      ),
    );
  }

  // // Widget for back button and evething inside the camera preview
  // Widget childLayout() {
  //   return SafeArea(
  //     child: Stack(
  //       children: [
  //         Container(
  //           padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
  //           child: ElevatedButton(
  //             onPressed: () {
  //               Navigator.pop(context);
  //             },
  //             child: Text(
  //               widget.backButtonText,
  //             ),
  //           ),
  //         ),
  //         // CircleAvatar(
  //         //   backgroundColor: !recording ? Colors.blueAccent : Colors.redAccent,
  //         //   child: Container(
  //         //     child: GestureDetector(
  //         //       onTap: !recording
  //         //           ? controller != null &&
  //         //                   controller.value.isInitialized &&
  //         //                   !controller.value.isRecordingVideo
  //         //               ? onVideoRecordButtonPressed
  //         //               : null
  //         //           : stopVideoRecording,
  //         //     ),
  //         //     decoration: BoxDecoration(
  //         //       color: const Color(0xff7c94b6),
  //         //       borderRadius: new BorderRadius.all(new Radius.circular(10.0)),
  //         //       border: new Border.all(
  //         //         color: Colors.white,
  //         //         width: 2.0,
  //         //       ),
  //         //     ),
  //         //   ),
  //         // ),
  //       ],
  //     ),
  //   );
  // }

  // void onVideoRecordButtonPressed() {
  //   setState(() {
  //     recording = true;
  //   });
  //   startVideoRecording().then((_) {
  //     if (mounted) setState(() {});
  //   });
  // }

  // void onStopButtonPressed() {
  //   stopVideoRecording().then((file) {
  //     if (mounted) setState(() {});
  //     if (file != null) {
  //       showInSnackBar('Video recorded to ${file.path}');
  //       videoFile = file;
  //       print(videoFile.path);
  //       // shareScreenshot(videoFile);

  //       setState(() {
  //         recording = false;
  //       });
  //     }
  //   });
  // }

  // Future<void> startVideoRecording() async {
  //   final CameraController cameraController = controller;

  //   if (cameraController == null || !cameraController.value.isInitialized) {
  //     showInSnackBar('Error: select a camera first.');
  //     return;
  //   }

  //   if (cameraController.value.isRecordingVideo) {
  //     // A recording is already started, do nothing.
  //     return;
  //   }

  //   try {
  //     await cameraController.startVideoRecording();
  //   } on CameraException catch (e) {
  //     _showCameraException(e);
  //     return;
  //   }
  // }

  // Future<XFile> stopVideoRecording() async {
  //   final CameraController cameraController = controller;

  //   if (cameraController == null || !cameraController.value.isRecordingVideo) {
  //     return null;
  //   }

  //   try {
  //     return cameraController.stopVideoRecording();
  //   } on CameraException catch (e) {
  //     _showCameraException(e);
  //     return null;
  //   }
  // }

  // void _showCameraException(CameraException e) {
  //   logError(e.code, e.description);
  //   showInSnackBar('Error: ${e.code}\n${e.description}');
  // }

  // Future<Null> shareScreenshot() async {
  //   try {
  //     RenderRepaintBoundary boundary =
  //         _globalKey.currentContext.findRenderObject();
  //     if (boundary.debugNeedsPaint) {
  //       Timer(Duration(seconds: 1), () => shareScreenshot());
  //       return null;
  //     }
  //     ui.Image image = await boundary.toImage();
  //     final directory = (await getExternalStorageDirectory()).path;
  //     ByteData byteData =
  //         await image.toByteData(format: ui.ImageByteFormat.png);
  //     Uint8List pngBytes = byteData.buffer.asUint8List();
  //     File imgFile = new File('$directory/screenshot.png');
  //     imgFile.writeAsBytes(pngBytes);
  //     final RenderBox box = context.findRenderObject();
  //     Share.shareFile(File('$directory/screenshot.png'),
  //         subject: 'Share ScreenShot',
  //         text: 'Hello, check your share files!',
  //         sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
  //   } on PlatformException catch (e) {
  //     print("Exception while taking screenshot:" + e.toString());
  //   }
  //   setState(() {
  //     button1 = false;
  //   });
  // }

  // void showInSnackBar(String message) {
  //   // ignore: deprecated_member_use
  //   Scaffold.of(context).showSnackBar(SnackBar(content: Text(message)));
  // }

  // void logError(String code, String message) {
  //   if (message != null) {
  //     print('Error: $code\nError Message: $message');
  //   } else {
  //     print('Error: $code');
  //   }
  // }
}
