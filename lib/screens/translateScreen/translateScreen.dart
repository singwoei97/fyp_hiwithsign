import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:fyp_hiwithsign/classes/screen_related/pageInformation.dart';
import 'package:fyp_hiwithsign/classes/screen_related/sizeInformation.dart';
import 'package:fyp_hiwithsign/providers/languageProvider.dart';
import 'package:fyp_hiwithsign/screen_template/screen_type_C_translate.dart';
import 'package:fyp_hiwithsign/screens/translateScreen/boundingBox.dart';
import 'package:fyp_hiwithsign/screens/translateScreen/translateCameraFeed.dart';
import 'package:provider/provider.dart';
import 'dart:math' as math;
import 'package:tflite/tflite.dart';

class TranslateScreen extends StatefulWidget {
  static String routeName = "translate";
  final PageInformation pageInfo;
  final List<CameraDescription> cameras;

  TranslateScreen(this.cameras, {this.pageInfo});

  @override
  _TranslateScreenState createState() => _TranslateScreenState();
}

class _TranslateScreenState extends State<TranslateScreen> {
  PageInformation pageInfo;
  List<dynamic> _recognitions;
  int _imageHeight = 0;
  int _imageWidth = 0;
  LanguageProvider languageProvider;
  initCameras() async {}
  loadTfModel() async {
    await Tflite.loadModel(
      model: "assets/tfliteFiles/model.tflite",
      labels: "assets/tfliteFiles/labels.txt",
    );
  }

  /* 
  The set recognitions function assigns the values of recognitions, imageHeight and width to the variables defined here as callback
  */
  setRecognitions(recognitions, imageHeight, imageWidth) {
    setState(() {
      _recognitions = recognitions;
      _imageHeight = imageHeight;
      _imageWidth = imageWidth;
    });
  }

  closeTfModel() async {
    await Tflite.close();
  }

  @override
  void dispose() {
    super.dispose();
    closeTfModel();
  }

  @override
  void initState() {
    pageInfo = PageInformation(
        fromPage: widget.pageInfo.currentPage,
        currentPage: widget.pageInfo.toPage);
    languageProvider = Provider.of<LanguageProvider>(context, listen: false);
    super.initState();
    loadTfModel();
  }

  @override
  Widget build(BuildContext context) {
    Size screen = MediaQuery.of(context).size;
    return ScreenC(
      builder: (BuildContext context, SizeInformation sizeInfo) {
        return Stack(
          children: <Widget>[
            TranslateCameraFeed(
              widget.cameras,
              setRecognitions,
              childLayout(
                languageProvider.s('back'),
              ),
            ),
            BoundingBox(
              _recognitions == null ? [] : _recognitions,
              math.max(_imageHeight, _imageWidth),
              math.min(_imageHeight, _imageWidth),
              screen.height,
              screen.width,
            ),
          ],
        );
      },
      pageInformation: pageInfo,
    );
  }

  // Widget for back button and evething inside the camera preview
  Widget childLayout(String buttonText) {
    return SafeArea(
      child: Stack(
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            child: ElevatedButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text(
                buttonText,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
