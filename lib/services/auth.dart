import 'package:firebase_auth/firebase_auth.dart';
import 'package:fyp_hiwithsign/classes/connection_related/serverResponse.dart';

class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  // sign in with email and password
  Future<ServerResponse> signInWithEmailAndPassword(
      String email, String password) async {
    try {
      AuthResult result = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      FirebaseUser user = result.user;
      return ServerResponse(
          success: true, data: user, message: "Successfully sign in.");
    } catch (e) {
      print(e.toString());
      return ServerResponse(success: false, message: e.toString());
    }
  }

  // register with email and password
  Future<ServerResponse> registerWithEmailAndPassword(
      String email, String password, String userName) async {
    try {
      AuthResult result = await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
      FirebaseUser user = result.user;
      var userUpdateInfo = UserUpdateInfo();
      userUpdateInfo.displayName = userName;
      user.updateProfile(userUpdateInfo);
      await user.reload();
      user = await _auth.currentUser();
      return ServerResponse(
          success: true, data: user, message: "Successfully register in.");
    } catch (e) {
      print(e.toString());
      return ServerResponse(success: false, message: e.toString());
    }
  }

  // sign out
  Future<ServerResponse> signOut() async {
    try {
      await _auth.signOut();
      return ServerResponse(success: true, message: "Successfully logged out.");
    } catch (e) {
      print(e.toString());
      return ServerResponse(success: false, message: e.toString());
    }
  }
}
