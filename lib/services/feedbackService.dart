import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fyp_hiwithsign/classes/connection_related/serverResponse.dart';
import 'package:fyp_hiwithsign/classes/model/user.dart';

class FeedbackService {
  final _firestoreInstance = Firestore.instance;

  // submit feedback
  Future<ServerResponse> submitFeedback(User user, String message) async {
    try {
      await _firestoreInstance.collection("Feedback").add({
        "userId": "${user.id}",
        "message": message,
        "dateSent": FieldValue.serverTimestamp(),
      });
      return ServerResponse(success: true, message: "Submitted.");
    } catch (e) {
      print(e.toString());
      return ServerResponse(success: false, message: e.toString());
    }
  }
}
