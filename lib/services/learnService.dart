import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fyp_hiwithsign/classes/model/category.dart';
import 'package:fyp_hiwithsign/classes/model/categoryItem.dart';
import 'package:fyp_hiwithsign/classes/model/sign.dart';
import 'package:fyp_hiwithsign/classes/model/video.dart';

class LearnService {
  final _firestoreInstance = Firestore.instance;

  Future<List<Category>> getCategory() async {
    try {
      var result = await _firestoreInstance.collection("Learn").getDocuments();
      return result.documents
          .map((document) => Category(
              categoryName: document.documentID.toString(),
              categoryExample: document.data["example"].toString()))
          .toList();
    } catch (e) {
      print(e.toString());
      return [];
    }
  }

  Future<List<CategoryItem>> getCategoryItems(String category) async {
    try {
      var result = await _firestoreInstance
          .collection("Learn")
          .document(category)
          .collection("CategoryItem")
          .getDocuments();
      return result.documents
          .map((document) => CategoryItem(
              itemID: document.documentID.toString(),
              itemName: document.data["name"].toString()))
          .toList();
    } catch (e) {
      print(e.toString());
      return [];
    }
  }

  Future<Sign> getLearnVideo(String categoryKey, String signDocumentKey) async {
    try {
      var result = await _firestoreInstance
          .collection("Learn")
          .document(categoryKey)
          .collection("CategoryItem")
          .document(signDocumentKey)
          .get();
      return Sign(
        signName: result.data["name"],
        category: categoryKey,
        video: Video(
          videoPath: result.data["videoLink"],
        ),
      );
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future<Video> getTutorialVideo(String tutorialDocumentID) async {
    try {
      var result = await _firestoreInstance
          .collection("Tutorial")
          .document(tutorialDocumentID)
          .get();
      return Video(
        videoName: result.data["name"],
        videoPath: result.data["videoLink"],
      );
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
}
