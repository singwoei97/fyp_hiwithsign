import 'package:flutter/material.dart';

/// Start *AppName*
String appName = "HiWithSign";

/// End   *AppName*

/// Start *App Color Section*
MaterialColor primaryColour = MaterialColor(0xffF9CECE, colorMap_0xffF9CECE);

Map<int, Color> colorMap_0xffF9CECE = {
  50: Color.fromRGBO(259, 206, 206, .1),
  100: Color.fromRGBO(259, 206, 206, .2),
  200: Color.fromRGBO(259, 206, 206, .3),
  300: Color.fromRGBO(259, 206, 206, .4),
  400: Color.fromRGBO(259, 206, 206, .5),
  500: Color.fromRGBO(259, 206, 206, .6),
  600: Color.fromRGBO(259, 206, 206, .7),
  700: Color.fromRGBO(259, 206, 206, .8),
  800: Color.fromRGBO(259, 206, 206, .9),
  900: Color.fromRGBO(259, 206, 206, 1),
};

/// End *App Color Section*

/// Start *Button Color Section*
Color primaryButtonColour = Color(0xffC318D2); // Purple
List<Color> buttonColours = [
  /// Brown orange like color
  Color(0xffD28818),

  /// Green color
  Color(0xff18D241),

  /// Blue color
  Color(0xff188FD2),

  /// Red color
  Color(0xffD21818)
];
Color settingButtonColour = Color(0xff500AE7);

List<Color> listTileItemsColor = [
  /// Light pink color
  Color(0xffFCF3F3),

  /// White color
  Color(0xffFAFAFA),
];

/// End *Button Color Section*

/// Start *App text color section*
Color textColorWhite = Color(0xffFFFFFF);
Color textColorDark = Color(0xff343547);

/// End *App text color section*

/// Start *Text style section*
TextStyle buttonTextStyle = TextStyle(fontSize: 30.0, fontFamily: 'RozhaOne');
TextStyle wordingTextStyle = TextStyle(fontSize: 40.0, fontFamily: 'RozhaOne');
TextStyle extraDescriptionTextStyle =
    TextStyle(fontSize: 30.0, fontFamily: 'RozhaOne');

/// End *Text style section*

/// Start *Original Development ScreenSize section*
Size originalDevelopmentScreenSize = Size(100, 100);

/// End *Original Development ScreenSize section*
