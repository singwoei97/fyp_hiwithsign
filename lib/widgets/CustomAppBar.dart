import 'package:flutter/material.dart';
import 'package:fyp_hiwithsign/classes/screen_related/pageInformation.dart';
import 'package:fyp_hiwithsign/helper/textHelper.dart';
import 'package:fyp_hiwithsign/providers/languageProvider.dart';
import 'package:fyp_hiwithsign/utils/globals.dart' as globals;
import 'package:provider/provider.dart';
// import 'package:fyp_hiwithsign/utils/language_constant.dart';
// import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class CustomAppBar extends StatefulWidget implements PreferredSizeWidget {
  static String routeName = "CustomAppBar";
  final PageInformation pageInformation;
  final bool automaticallyImplyLeading;
  final double elevation;
  final bool settingButtonOn;
  final bool onlyBackButton;
  final Text extraDescription;
  final Color extraDescriptionBackground;
  final Function settingButtonCustomFunction;
  final Widget customSettingButton;

  CustomAppBar(
      {Key key,
      this.pageInformation,
      this.automaticallyImplyLeading = false,
      this.elevation = 0,
      this.settingButtonOn = false,
      this.onlyBackButton = false,
      this.extraDescription,
      this.extraDescriptionBackground,
      this.settingButtonCustomFunction,
      this.customSettingButton})
      : super(key: key);

  @override
  Size get preferredSize =>
      Size.fromHeight(((extraDescription != null) || onlyBackButton) == true
          ? kToolbarHeight * 2
          : kToolbarHeight);

  @override
  _CustomAppBarState createState() => _CustomAppBarState();
}

class _CustomAppBarState extends State<CustomAppBar> {
  LanguageProvider languageProvider;
  @override
  Widget build(BuildContext context) {
    // print(TextHelper.removeSubInfo(widget.pageInformation.currentPage));
    languageProvider = Provider.of<LanguageProvider>(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        widget.onlyBackButton == true
            ? onlyBackButtonRequired(context)
            : Stack(
                children: [
                  widget.extraDescription != null
                      ? Align(
                          alignment: Alignment.topRight,
                          child: Container(
                            margin: EdgeInsets.symmetric(
                                vertical: 5, horizontal: 5),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30.0),
                              color: widget.extraDescriptionBackground ??
                                  Colors.transparent,
                            ),
                            height: kToolbarHeight * 1.6,
                            width: kToolbarHeight * 2.9,
                            child: Align(
                                alignment: Alignment.bottomCenter,
                                child: Container(
                                    margin: EdgeInsets.symmetric(
                                        vertical: 5, horizontal: 5),
                                    child: widget.extraDescription ??
                                        Container())),
                          ),
                        )
                      : Container(),
                  // Localizations(
                  //   delegates: [
                  //     MyLocalizations.delegate,
                  //     GlobalMaterialLocalizations.delegate,
                  //     GlobalWidgetsLocalizations.delegate,
                  //     GlobalCupertinoLocalizations.delegate,
                  //   ],
                  //   locale: MyApp.getCurrentLocale(context),
                  //   child:

                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          child: Text(
                            TextHelper.capitalize(
                              TextHelper.removeSubInfo(
                                  widget.pageInformation.currentPage),
                            ),
                            style: TextStyle(
                                fontSize: 25.0, fontFamily: 'RozhaOne'),
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: widget.settingButtonOn == true
                            ? settingButton(context)
                            : Container(),
                      )
                    ],
                  ),
                  // AppBar(
                  //   backgroundColor: Colors.transparent,
                  //   actions: <Widget>[
                  //     widget.settingButtonOn == true
                  //         ? settingButton(context)
                  //         : Container(),
                  //   ],
                  //   automaticallyImplyLeading:
                  //       widget.automaticallyImplyLeading,
                  //   title: Align(
                  //     alignment: Alignment.topLeft,
                  //     child: Stack(
                  //       children: [
                  //         Align(
                  //           alignment: Alignment.centerLeft,
                  //           child: Container(
                  //             padding: EdgeInsets.symmetric(horizontal: 10),
                  //             child: Text(
                  //               TextHelper.capitalize(
                  //                 TextHelper.removeSubInfo(
                  //                     widget.pageInformation.currentPage),
                  //               ),
                  //               style: TextStyle(
                  //                   fontSize: 25.0, fontFamily: 'RozhaOne'),
                  //             ),
                  //           ),
                  //         ),
                  //       ],
                  //     ),
                  //   ),
                  //   elevation: widget.elevation,
                  // ),
                ],
              ),
      ],
    );
  }

  Widget onlyBackButtonRequired(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: ElevatedButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
            child: Text(
              // AppLocalizations.of(context).back,
              languageProvider.s('back'),
              style: TextStyle(fontSize: 25.0, fontFamily: 'RozhaOne'),
            ),
          )),
    );
  }

  Widget settingButton(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: widget.customSettingButton ??
          CircleAvatar(
            backgroundColor: globals.settingButtonColour,
            child: IconButton(
              icon: Icon(
                Icons.settings,
                color: Colors.white,
              ),
              onPressed: widget.settingButtonCustomFunction ?? () {},
            ),
          ),
    );
  }
}
