import 'package:flutter/material.dart';

enum DialogBoxType { normal, loading }

Future<bool> alertDialog(
  BuildContext context, {
  String title,
  String content,
  DialogBoxType dialogBoxType,
}) {
  bool isLoading = dialogBoxType == DialogBoxType.loading;
  Widget titleWidget = (isLoading ? Container() : Text(title ?? "Error"));
  Widget contentWidget = isLoading
      ? Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
              CircularProgressIndicator(
                backgroundColor: Colors.white,
              ),
            ])
      : Text(content ?? "No message available");
  return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) => AlertDialog(
          elevation: isLoading ? 0 : 10,
          backgroundColor: isLoading ? Colors.transparent : Colors.white,
          title: titleWidget,
          content: contentWidget,
          actions: <Widget>[
            dialogBoxType == DialogBoxType.loading
                ? Container()
                : ElevatedButton(
                    onPressed: () => Navigator.of(context).pop(true),
                    child: Text("Ok"),
                  ),
          ],
        ),
      ) ??
      false;
}
