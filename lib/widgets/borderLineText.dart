import 'package:flutter/material.dart';
import 'package:fyp_hiwithsign/utils/globals.dart' as globals;

class BorderText extends StatefulWidget {
  final String text;
  final double fontSize;
  final Color fontColor;
  final Color borderColor;

  const BorderText(this.text,
      {Key key, this.fontSize = 40, this.fontColor, this.borderColor})
      : super(key: key);

  @override
  _BorderTextState createState() => _BorderTextState();
}

class _BorderTextState extends State<BorderText> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        // Stroked text as border.
        Text(
          widget.text,
          style: TextStyle(
            fontSize: widget.fontSize,
            foreground: Paint()
              ..style = PaintingStyle.stroke
              ..strokeWidth = 3
              ..color = widget.borderColor ?? globals.textColorDark,
          ),
        ),
        // Solid text as fill.
        Text(
          widget.text,
          style: TextStyle(
            fontSize: widget.fontSize,
            color: widget.fontColor ?? globals.textColorWhite,
          ),
        ),
      ],
    );
  }
}
