import 'package:flutter/material.dart';
import 'package:fyp_hiwithsign/helper/textHelper.dart';
import 'package:fyp_hiwithsign/utils/globals.dart' as globals;

class LearnCategoryListTile extends StatefulWidget {
  final String category;
  final String categoryExample;
  final Function onPressed;
  final int index;

  LearnCategoryListTile(this.index,
      {@required this.onPressed, this.category, this.categoryExample});

  @override
  _LearnCategoryListTileState createState() => _LearnCategoryListTileState();
}

class _LearnCategoryListTileState extends State<LearnCategoryListTile> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Align(
            alignment: (widget.index % 2 == 0)
                ? Alignment.centerLeft
                : Alignment.centerRight,
            child: Text(
              widget.categoryExample.length > 10
                  ? TextHelper.newlineThethirdItem(widget.categoryExample)
                  : widget.categoryExample,
              style: TextStyle(fontSize: 30.0, fontFamily: 'RozhaOne'),
            ),
          ),
          Align(
            alignment: (widget.index % 2 == 0)
                ? Alignment.centerRight
                : Alignment.centerLeft,
            child: ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(
                    globals.buttonColours[calculateIndex()]),
              ),
              onPressed: widget.onPressed ??
                  () {
                    print(((widget.index + 1) > (globals.buttonColours.length))
                        ? ((widget.index + 1) %
                                (globals.buttonColours.length)) -
                            1
                        : widget.index);
                  },
              child: Container(
                margin: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                child: Text(
                  widget.category,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  calculateIndex() {
    int x = (widget.index + 1) % (globals.buttonColours.length);
    return x == 0 ? globals.buttonColours.length - 1 : x - 1;
  }
}
