import 'package:flutter/material.dart';
import 'package:fyp_hiwithsign/utils/globals.dart' as globals;
import 'package:fyp_hiwithsign/widgets/borderLineText.dart';

class LearnItemsListTile extends StatefulWidget {
  final String items;
  final Function onPressed;
  final int index;

  LearnItemsListTile(this.index, {this.items, @required this.onPressed});

  @override
  _LearnItemsListTileState createState() => _LearnItemsListTileState();
}

class _LearnItemsListTileState extends State<LearnItemsListTile> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: FlatButton(
        color: globals.listTileItemsColor[widget.index % 2 == 0 ? 0 : 1],
        onPressed: widget.onPressed ??
            () {
              print(((widget.index + 1) > (globals.buttonColours.length))
                  ? ((widget.index + 1) % (globals.buttonColours.length)) - 1
                  : widget.index);
            },
        child: Container(
          child: Row(
            children: [
              SizedBox(
                width: 15,
              ),
              BorderText(widget.items),
            ],
          ),
        ),
      ),
    );
  }
}
