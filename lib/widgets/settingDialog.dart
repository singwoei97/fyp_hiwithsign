import 'package:flutter/material.dart';
import 'package:fyp_hiwithsign/providers/languageProvider.dart';
import 'package:provider/provider.dart';
// import 'package:flutter_gen/gen_l10n/app_localizations.dart';
// import 'package:fyp_hiwithsign/main.dart';

class SettingDialog extends StatefulWidget {
  const SettingDialog({Key key, this.alertText}) : super(key: key);
  final String alertText;

  @override
  _SettingDialogState createState() => _SettingDialogState();
}

class _SettingDialogState extends State<SettingDialog> {
  @override
  Widget build(BuildContext context) {
    var languageProvider = Provider.of<LanguageProvider>(context);
    return AlertDialog(
      title: Text(
        widget.alertText,
      ),
      content: ListView(
        children: <Widget>[
          GestureDetector(
            onTap: () async {
              // _changeLanguage('en', context);
              languageProvider.setLocale('en');
              Navigator.pop(context);
            },
            child: ListTile(
              title: Text('English'),
            ),
          ),
          GestureDetector(
            onTap: () async {
              // _changeLanguage('bm', context);
              languageProvider.setLocale('bm');
              Navigator.pop(context);
            },
            child: ListTile(
              title: Text('Malay'),
            ),
          ),
        ],
      ),
    );
  }

  // void _changeLanguage(String languageCode, context) async {
  //   Locale locale = Locale(languageCode);
  //   MyApp.of(this.context).setLocale(locale);
  // }
}
