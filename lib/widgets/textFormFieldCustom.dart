import 'package:flutter/material.dart';

class TextFormFieldCustom extends StatefulWidget {
  final TextEditingController textEditingController;
  final TextInputType textInputType;
  final String hintText;
  final bool obscureText;
  final String title;
  final TextInputAction textInputAction;
  final FocusNode focusNode;
  final int maxlines;

  TextFormFieldCustom(this.textEditingController,
      {this.textInputType,
      this.hintText = "",
      this.obscureText = false,
      this.title = "",
      this.textInputAction = TextInputAction.next,
      this.focusNode,
      this.maxlines});

  @override
  _TextFormFieldCustomState createState() => _TextFormFieldCustomState();
}

class _TextFormFieldCustomState extends State<TextFormFieldCustom> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      maxLines: widget.maxlines ?? 1,
      focusNode: widget.focusNode,
      controller: widget.textEditingController,
      textInputAction: widget.textInputAction,
      keyboardType: widget.textInputType,
      obscureText: widget.obscureText,
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.white,
        contentPadding: EdgeInsets.only(left: 10, right: 10),
        hintText: widget.hintText,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
      ),
    );
  }
}
